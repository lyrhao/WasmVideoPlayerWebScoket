# WasmVideoPlayerWebScoket

#### Description
基于WASM的H265 Web播放器，web无插件解码播放H264/H265(js解码HTML5播放)。支持http协议，webscoket协议。参考：https://github.com/sonysuqin/WasmVideoPlayer
依赖:
1. WASM
可以在浏览器里执行原生代码(例如C、C++)，要开发可以在浏览器运行的原生代码，需要安装他的工具链。
2.FFmpeg
主要使用FFmpeg来做解封装(demux)和解码(decoder)，使用Emscripten编译FFmpeg
3.WebGL
H5使用Canvas来绘图，但是默认的2d模式只能绘制RGB格式，使用FFmpeg解码出来的视频数据是YUV格式，想要渲染出来需要进行颜色空间转换，可以使用FFmpeg的libswscale模块进行转换。
4 Web Audio
FFmpeg解码出来的音频数据是PCM格式，可以使用H5的Web Audio Api来播放。


#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
